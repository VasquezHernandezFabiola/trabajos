/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.Serializable;
import java.util.Random;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author fabi_
 */
@Named(value = "numero")
@ApplicationScoped
public class Numero implements Serializable {
    private int numeroa;
    private int numero;
    private String mensaje = "";

    /**
     * Creates a new instance of Numero
     */
    public Numero() {
        Random na = new Random();
        numero = na.nextInt(100)+1;
        System.out.print("ESTE ES EL NUMERO"+numero);
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getMensaje() {
        return mensaje;
    }
    
    public void mensajem(){
        if(numeroa>numero)
            mensaje = "Debe ser un valor menor";
        else if(numeroa<numero)
            mensaje = "Debe ser un valor mayor";
        else
            mensaje = "ACERTO";
    }

    public int getNumeroa() {
        return numeroa;
    }

    public void setNumeroa(int numeroa) {
        this.numeroa = numeroa;
    }
    
}
