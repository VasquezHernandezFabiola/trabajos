/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.Random;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fabi_
 */
@Named(value = "juego")
@SessionScoped
public class Juego implements Serializable {
    private Integer valorInicio;
    private Integer valorFinal;
    private Integer numIntentos;
    
    private Integer valorUsuario = 0;
    private Integer valorAleatorio;
    private String mensaje = "";
    
    /**
     * Creates a new instance of Juego
     */
    public Juego() {
        Random na = new Random();
        valorAleatorio = valorInicio + na.nextInt(valorFinal);
        System.out.print("ESTE ES EL NUMERO"+ valorAleatorio);
    }
    
    public String getMensaje() {
        return mensaje;
    }
    
    public void mensajem(){
        if(valorUsuario>valorAleatorio)
            mensaje = "Debe ser un valor menor";
        else if(valorUsuario<valorAleatorio)
            mensaje = "Debe ser un valor mayor";
        else
            mensaje = "ACERTO";
    }
    
    public void mensajeD(){
       this.setNumIntentos(this.getNumIntentos()-1);
       
       if(numIntentos>0){
        if(valorUsuario==valorAleatorio){mensaje = "Acertaste";}
        else{mensaje = "El valor debe ser " + (valorAleatorio>valorUsuario ? "mayor a " : "menor a ") + valorUsuario;}
       }else{
           mensaje = "Se acabaron tus intentos";
       }
    }
    
    public Integer getValor() {
        return valorUsuario;
    }
    public void setValor(Integer valor) {
        this.valorUsuario = valor;
    }
    public Integer getValorI() {
        return valorInicio;
    
    }
    public void setValorI(Integer valorI) {
        valorInicio = valorI;
    }
    public Integer getValorF() {
        return valorFinal;
    
    }
    public void setValorF(Integer valorF) {
        valorFinal = valorF;
    }
    
    public Integer getNumIntentos() {
        return numIntentos;
    }
    public void setNumIntentos(Integer valor) {
        numIntentos = valor;
    }
    
}
